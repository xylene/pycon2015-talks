
Reusable Code and Beyond PEP 8

* Greg Ward - How to Write Reusable Code [https://www.youtube.com/watch?v=r9cnHO15YgU]
(https://www.youtube.com/watch?v=r9cnHO15YgU)
* Raymond Hettinger - Beyond PEP 8 -- Best Practices for Beautiful Code
[https://www.youtube.com/watch?v=wf-BqAjZb8M](https://www.youtube.com/watch?v=wf-BqAjZb8M)
* Raymond Hettinger - Transforming Code into Beautiful, Idiomatic Python
[https://www.youtube.com/watch?v=OSGv2VnC0go](https://www.youtube.com/watch?v=OSGv2VnC0go)


