# List of Links to Videos Referenced at Post PyCon Talks 2015

Bloom Filters

* Curtis Lassam - Hash Functions and You: Partners in Freedom [https://www.youtube.com/watch?v=IGwNQfjLTp0
](https://www.youtube.com/watch?v=IGwNQfjLTp0)
* Alex Gaynor - Fast Python, Slow Python
  [https://www.youtube.com/watch?v=7eeEf_rAJds](https://www.youtube.com/watch?v=7eeEf_rAJds)
